package br.com.galgo.testes.suite;

import org.junit.experimental.categories.Categories;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import br.com.galgo.cumpre_eventos_pendentes.CumprirEventosPedentesPlCota;

@RunWith(Categories.class)
@Suite.SuiteClasses({ CumprirEventosPedentesPlCota.class })
public class SuiteEventosPendentesPlCota {

}
